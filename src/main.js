import Vue from 'vue'
import './plugins/vuetify'
import App from './App.vue'
import Vuex from 'vuex'

Vue.use(Vuex);
Vue.config.productionTip = false

const store = new Vuex.Store({
  state: {
    server: 'https://vonk.fire.ly',
  },
  mutations: {
    server (state, data) {
      state.server = data
    },
  }
});

new Vue({
  render: h => h(App),
  store,
}).$mount('#app')
